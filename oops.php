<!--objects and classes-->

<?php 

echo "class and object";

class TV{

    public $model='xyz';
    public $volume=1;

    function volumeUp()
    {
        $this->volume++;
    }

    function volumeDown(){
        $this->volume--;
    }



}

$tv_one =new TV;
$tv_two = new TV;

$tv_one->volumeUp();
$tv_two->volumeDown();


echo $tv_one->volume."<br>";
echo $tv_two->volume."<br>";
echo $tv_one->model;


?>